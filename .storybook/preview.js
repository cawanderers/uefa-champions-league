import 'bootstrap/dist/css/bootstrap.css';
export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  backgrounds: {
    default: 'dark',
    values: [{ name: 'dark', value: '#0a14a6'}]
  }
}
