import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders uefa link', () => {
  render(<App />);
  const headerElement = screen.getByText(/UEFA GROUP STAGE TEAMS/i);
  expect(headerElement).toBeInTheDocument();
});
