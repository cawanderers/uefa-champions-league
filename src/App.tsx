import React from 'react';
import Navbar from './components/Navbar';
import Clubs from "./components/Clubs/Clubs";
import Matches from "./components/Matches/Matches";
import Players from "./components/Players/Players";
import Standings from "./components/Standings/Standings";
import GoalRanking from "./components/GoalRanking/GoalRanking";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Matches />
      <Clubs />
      <Players />
      <Standings />
      <GoalRanking />
    </div>
  );
}

export default App;
