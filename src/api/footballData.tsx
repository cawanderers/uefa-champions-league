import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.football-data.org/v2',
  headers: {'X-Auth-Token': '29d79cbaee294ddfbeee727b4ad2b9e1'}
});
