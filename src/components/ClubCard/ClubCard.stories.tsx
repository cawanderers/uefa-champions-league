import React from 'react';
import { Story } from '@storybook/react/types-6-0';

import ClubCard from './ClubCard';

export default {
  title: 'ClubCard',
  component: ClubCard,
};

const Template: Story<any> = (args: {name: string, logo: string}) => (
  <ClubCard {...args} />
);

export const Default = Template.bind({});
Default.args = {
  name: 'Zenit', logo: `${process.env.PUBLIC_URL}/assets/images/ClubsPng/${731}.png`
};
