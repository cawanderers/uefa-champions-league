import React from 'react';

const ClubCard = ({name, logo}: { name: string, logo: string }) => {
  return (
    <div className="card col" style={{width: "18rem"}}>
      <img className="card-img-top mx-auto mt-5" src={logo} alt={name} />
      <div className="card-body">
        <p className="card-text text-center">{name}</p>
      </div>
    </div>
  );
};

export default ClubCard;
