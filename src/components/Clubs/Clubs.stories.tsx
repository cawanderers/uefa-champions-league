import React from 'react';
import { Story } from '@storybook/react/types-6-0';

import Clubs from './Clubs';

export default {
  title: 'Clubs',
  component: Clubs,
};

const Template: Story<any> = (args) => (
  <Clubs {...args} />
);

export const Default = Template.bind({});
Default.args = {
  "teams": [
    {
      "id": 731,
      "area": {
        "id": 2195,
        "name": "Russia"
      },
      "name": "FK Zenit Sankt-Petersburg",
      "shortName": "Zenit",
      "tla": "ZEN",
      "crestUrl": "https://crests.football-data.org/731.svg",
      "address": "Pr. Dobrolyubova 16/2-А Sankt-Petersburg 197198",
      "phone": "+7 (812) 2448888",
      "website": "http://www.fc-zenit.ru",
      "email": "office@fc-zenit.ru",
      "founded": 1925,
      "clubColors": "Blue / White",
      "venue": "Gazprom Arena",
      "lastUpdated": "2020-11-19T02:16:16Z"
    },
    {
      "id": 851,
      "area": {
        "id": 2023,
        "name": "Belgium"
      },
      "name": "Club Brugge KV",
      "shortName": "Club Brugge",
      "tla": "CLU",
      "crestUrl": "https://crests.football-data.org/851.svg",
      "address": "Olympialaan 74 Brugge 8200",
      "phone": "+32 (50) 402121",
      "website": "http://www.clubbrugge.be",
      "email": "info@clubbrugge.be",
      "founded": 1894,
      "clubColors": "Blue / Black",
      "venue": "Jan Breydelstadion",
      "lastUpdated": "2020-11-19T02:17:35Z"
    },
    {
      "id": 842,
      "area": {
        "id": 2253,
        "name": "Ukraine"
      },
      "name": "FK Dynamo Kyiv",
      "shortName": "Dynamo Kyiv",
      "tla": "DYN",
      "crestUrl": "https://crests.football-data.org/842.svg",
      "address": "Vulitsa Grushevskogo, 3 Kyïv 01001",
      "phone": "+380 (044) 5970008",
      "website": "http://www.fcdynamo.kiev.ua",
      "email": "semenenko@goal.com.ua",
      "founded": 1927,
      "clubColors": "White / Blue",
      "venue": "NSK Olimpijs'kyj",
      "lastUpdated": "2020-11-19T02:10:09Z"
    },
    {
      "id": 109,
      "area": {
        "id": 2114,
        "name": "Italy"
      },
      "name": "Juventus FC",
      "shortName": "Juventus",
      "tla": "JUV",
      "crestUrl": "https://crests.football-data.org/109.svg",
      "address": "Corso Galileo Ferraris, 32 Torino 10128",
      "phone": "+39 (011) 65631",
      "website": "http://www.juventus.com",
      "email": "francesco.gianello@juventus.com",
      "founded": 1897,
      "clubColors": "White / Black",
      "venue": "Allianz Stadium",
      "lastUpdated": "2020-11-19T02:14:53Z"
    },
    {
      "id": 61,
      "area": {
        "id": 2072,
        "name": "England"
      },
      "name": "Chelsea FC",
      "shortName": "Chelsea",
      "tla": "CHE",
      "crestUrl": "https://crests.football-data.org/61.svg",
      "address": "Fulham Road London SW6 1HS",
      "phone": "+44 (0871) 9841955",
      "website": "http://www.chelseafc.com",
      "email": null,
      "founded": 1905,
      "clubColors": "Royal Blue / White",
      "venue": "Stamford Bridge",
      "lastUpdated": "2020-11-19T02:08:09Z"
    },
    {
      "id": 559,
      "area": {
        "id": 2224,
        "name": "Spain"
      },
      "name": "Sevilla FC",
      "shortName": "Sevilla FC",
      "tla": "SEV",
      "crestUrl": "https://crests.football-data.org/559.svg",
      "address": "Calle Sevilla Fútbol Club, s/n Sevilla 41005",
      "phone": "+34 (902) 510011",
      "website": "http://www.sevillafc.es",
      "email": "sevillafc@sevillafc.es",
      "founded": 1905,
      "clubColors": "White / Red",
      "venue": "Estadio Ramón Sánchez Pizjuán",
      "lastUpdated": "2020-11-19T02:13:58Z"
    },
    {
      "id": 529,
      "area": {
        "id": 2081,
        "name": "France"
      },
      "name": "Stade Rennais FC 1901",
      "shortName": "Stade Rennais",
      "tla": "REN",
      "crestUrl": "https://crests.football-data.org/529.svg",
      "address": "La Piverdière - Chemin de la Taupinais CS 5390 Rennes 35039",
      "phone": "+33 (0820) 000035",
      "website": "http://www.staderennais.com",
      "email": "4500015@footlbf.fr",
      "founded": 1901,
      "clubColors": "Red / Black",
      "venue": "Roazhon Park",
      "lastUpdated": "2020-11-19T02:00:10Z"
    },
    {
      "id": 5452,
      "area": {
        "id": 2195,
        "name": "Russia"
      },
      "name": "FK Krasnodar",
      "shortName": "Krasnodar",
      "tla": "KRA",
      "crestUrl": "https://crests.football-data.org/5452.svg",
      "address": "114 ul.Zhloby Krasnodar 350901",
      "phone": "+7 (861) 2108990",
      "website": "http://www.fckrasnodar.ru",
      "email": "info@fckrasnodar.ru",
      "founded": 2008,
      "clubColors": "Black / Green",
      "venue": "Stadion Akademii FK Krasnodar",
      "lastUpdated": "2020-11-19T02:07:52Z"
    },
    {
      "id": 110,
      "area": {
        "id": 2114,
        "name": "Italy"
      },
      "name": "SS Lazio",
      "shortName": "Lazio",
      "tla": "LAZ",
      "crestUrl": "https://crests.football-data.org/110.svg",
      "address": "Via di Santa Cornelia, 1000 Formello 00060",
      "phone": "+39 (06) 976071",
      "website": "http://www.sslazio.it",
      "email": "segreteria.lazio@sslazio.it",
      "founded": 1900,
      "clubColors": "White / Sky Blue",
      "venue": "Stadio Olimpico",
      "lastUpdated": "2020-11-19T02:14:54Z"
    },
    {
      "id": 4,
      "area": {
        "id": 2088,
        "name": "Germany"
      },
      "name": "BV Borussia 09 Dortmund",
      "shortName": "Dortmund",
      "tla": "BVB",
      "crestUrl": "https://crests.football-data.org/4.svg",
      "address": "Rheinlanddamm 207-209 Dortmund 44137",
      "phone": "+49 (231) 90200",
      "website": "http://www.bvb.de",
      "email": "info@bvb.de",
      "founded": 1909,
      "clubColors": "Black / Yellow",
      "venue": "Signal Iduna Park",
      "lastUpdated": "2020-11-19T02:15:32Z"
    },
    {
      "id": 81,
      "area": {
        "id": 2224,
        "name": "Spain"
      },
      "name": "FC Barcelona",
      "shortName": "Barça",
      "tla": "FCB",
      "crestUrl": "https://crests.football-data.org/81.svg",
      "address": "Avenida Arístides Maillol s/n Barcelona 08028",
      "phone": "+34 (902) 189900",
      "website": "http://www.fcbarcelona.com",
      "email": "secretaria@fcbarcelona.com",
      "founded": 1899,
      "clubColors": "Red / Navy Blue / Orange",
      "venue": "Camp Nou",
      "lastUpdated": "2020-11-19T02:13:35Z"
    },
    {
      "id": 5954,
      "area": {
        "id": 2106,
        "name": "Hungary"
      },
      "name": "Ferencvárosi TC",
      "shortName": "Ferencváros",
      "tla": "FTC",
      "crestUrl": "https://crests.football-data.org/5954.svg",
      "address": "Üllöi út 129. Budapest 1091",
      "phone": "+36 (1) 2156023",
      "website": "http://www.fradi.hu",
      "email": "ftcrt@ftc.hu",
      "founded": 1899,
      "clubColors": "Green / White",
      "venue": "Groupama Arena",
      "lastUpdated": "2020-11-19T02:06:39Z"
    },
    {
      "id": 524,
      "area": {
        "id": 2081,
        "name": "France"
      },
      "name": "Paris Saint-Germain FC",
      "shortName": "PSG",
      "tla": "PSG",
      "crestUrl": "https://crests.football-data.org/524.svg",
      "address": "24, rue de Commandant Guibaud Paris 7501",
      "phone": "+33 (0139) 733467",
      "website": "http://www.psg.fr",
      "email": "communaute@psg.fr",
      "founded": 1904,
      "clubColors": "Red / Blue / White",
      "venue": "Parc des Princes",
      "lastUpdated": "2020-11-19T02:18:00Z"
    },
    {
      "id": 66,
      "area": {
        "id": 2072,
        "name": "England"
      },
      "name": "Manchester United FC",
      "shortName": "Man United",
      "tla": "MUN",
      "crestUrl": "https://crests.football-data.org/66.svg",
      "address": "Sir Matt Busby Way Manchester M16 0RA",
      "phone": "+44 (0161) 8688000",
      "website": "http://www.manutd.com",
      "email": "enquiries@manutd.co.uk",
      "founded": 1878,
      "clubColors": "Red / White",
      "venue": "Old Trafford",
      "lastUpdated": "2020-11-19T02:08:15Z"
    },
    {
      "id": 721,
      "area": {
        "id": 2088,
        "name": "Germany"
      },
      "name": "RB Leipzig",
      "shortName": "RB Leipzig",
      "tla": "RBL",
      "crestUrl": "https://crests.football-data.org/721.svg",
      "address": "Neumarkt 29-33 Leipzig 04109",
      "phone": "+49 (0341) 1247970",
      "website": "http://www.dierotenbullen.com",
      "email": "soccerrbl.office@redbulls.com",
      "founded": 2009,
      "clubColors": "White / Red",
      "venue": "Red Bull Arena",
      "lastUpdated": "2020-11-19T02:02:08Z"
    },
    {
      "id": 1897,
      "area": {
        "id": 2247,
        "name": "Turkey"
      },
      "name": "Medipol Başakşehir FK",
      "shortName": "Başakşehir",
      "tla": "BAS",
      "crestUrl": "https://crests.football-data.org/1897.svg",
      "address": "Yunus Emre Caddesi, Başakşehir Stadı 4.Etap, Başakşehir İstanbul 34200",
      "phone": "+90 (212) 4733333",
      "website": "http://www.ibfk.com.tr",
      "email": "info@ibfk.org",
      "founded": 1990,
      "clubColors": "Orange / Blue",
      "venue": "Başakşehir Fatih Terim Stadium",
      "lastUpdated": "2020-11-19T02:14:20Z"
    },
    {
      "id": 1877,
      "area": {
        "id": 2016,
        "name": "Austria"
      },
      "name": "FC Red Bull Salzburg",
      "shortName": "RB Salzburg",
      "tla": "RBS",
      "crestUrl": "https://crests.football-data.org/1877.svg",
      "address": "Stadionstr. 2/3 Wals-Siezenheim 5071",
      "phone": "+43 (662) 433332",
      "website": "http://www.redbulls.com/de",
      "email": "soccerrbs.office@redbulls.com",
      "founded": 1933,
      "clubColors": "White / Red",
      "venue": "Red Bull Arena",
      "lastUpdated": "2020-11-19T02:11:26Z"
    },
    {
      "id": 5455,
      "area": {
        "id": 2195,
        "name": "Russia"
      },
      "name": "FK Lokomotiv Moskva",
      "shortName": "Lok Moskva",
      "tla": "LOK",
      "crestUrl": "https://crests.football-data.org/5455.svg",
      "address": "Bolshaya Cherkizovskaya, 125 А Moskva 107553",
      "phone": "+7 (495) 1619704",
      "website": "http://www.fclm.ru",
      "email": "info@fclm.ru",
      "founded": 1923,
      "clubColors": "Red / Green / White",
      "venue": "RZD Arena",
      "lastUpdated": "2020-11-19T02:07:54Z"
    },
    {
      "id": 86,
      "area": {
        "id": 2224,
        "name": "Spain"
      },
      "name": "Real Madrid CF",
      "shortName": "Real Madrid",
      "tla": "RMA",
      "crestUrl": "https://crests.football-data.org/86.svg",
      "address": "Avenida Concha Espina, 1 Madrid 28036",
      "phone": "+34 (913) 984300",
      "website": "http://www.realmadrid.com",
      "email": "atencionpublico@corp.realmadrid.com",
      "founded": 1902,
      "clubColors": "White / Purple",
      "venue": "Estadio Alfredo Di Stéfano",
      "lastUpdated": "2020-11-19T02:13:40Z"
    },
    {
      "id": 1887,
      "area": {
        "id": 2253,
        "name": "Ukraine"
      },
      "name": "FK Shakhtar Donetsk",
      "shortName": "Shaktar",
      "tla": "SHD",
      "crestUrl": "https://crests.football-data.org/1887.svg",
      "address": "vul. Artema, 86-a Donets’k 83050",
      "phone": "+380 (062) 3349906",
      "website": "https://shakhtar.com/",
      "email": "feedback@shakhtar.com",
      "founded": 1936,
      "clubColors": "Orange / Black",
      "venue": "Oblasny SportKomplex Metalist",
      "lastUpdated": "2020-11-19T02:10:11Z"
    },
    {
      "id": 5,
      "area": {
        "id": 2088,
        "name": "Germany"
      },
      "name": "FC Bayern München",
      "shortName": "Bayern M",
      "tla": "FCB",
      "crestUrl": "https://crests.football-data.org/5.svg",
      "address": "Säbenerstr. 51 München 81547",
      "phone": "+49 (089) 699310",
      "website": "http://www.fcbayern.de",
      "email": "service-team@fcb.de",
      "founded": 1900,
      "clubColors": "Red / White / Blue",
      "venue": "Allianz Arena",
      "lastUpdated": "2020-11-19T02:01:50Z"
    },
    {
      "id": 78,
      "area": {
        "id": 2224,
        "name": "Spain"
      },
      "name": "Club Atlético de Madrid",
      "shortName": "Atleti",
      "tla": "ATM",
      "crestUrl": "https://crests.football-data.org/78.svg",
      "address": "Paseo Virgen del Puerto, 67 Madrid 28005",
      "phone": "+34 (913) 669048",
      "website": "http://www.clubatleticodemadrid.com",
      "email": "comunicacion@clubatleticodemadrid.com",
      "founded": 1903,
      "clubColors": "Red / White / Blue",
      "venue": "Estadio Wanda Metropolitano",
      "lastUpdated": "2020-11-19T02:13:32Z"
    },
    {
      "id": 108,
      "area": {
        "id": 2114,
        "name": "Italy"
      },
      "name": "FC Internazionale Milano",
      "shortName": "Inter",
      "tla": "INT",
      "crestUrl": "https://crests.football-data.org/108.svg",
      "address": "Corso Vittorio Emanuele II 9 Milano 20122",
      "phone": "+39 (02) 77151",
      "website": "http://www.inter.it",
      "email": "segreteriaccic@inter.it",
      "founded": 1908,
      "clubColors": "Blue / Black",
      "venue": "Stadio Giuseppe Meazza",
      "lastUpdated": "2020-11-19T02:14:52Z"
    },
    {
      "id": 18,
      "area": {
        "id": 2088,
        "name": "Germany"
      },
      "name": "Borussia Mönchengladbach",
      "shortName": "M'gladbach",
      "tla": "BMG",
      "crestUrl": "https://crests.football-data.org/18.svg",
      "address": "Hennes-Weisweiler-Allee 1 Mönchengladbach 41179",
      "phone": "+49 (02161) 92930",
      "website": "http://www.borussia.de",
      "email": "info@borussia.de",
      "founded": 1900,
      "clubColors": "Black / White / Green",
      "venue": "Stadion im Borussia-Park",
      "lastUpdated": "2020-11-19T02:02:03Z"
    },
    {
      "id": 65,
      "area": {
        "id": 2072,
        "name": "England"
      },
      "name": "Manchester City FC",
      "shortName": "Man City",
      "tla": "MCI",
      "crestUrl": "https://crests.football-data.org/65.svg",
      "address": "SportCity Manchester M11 3FF",
      "phone": "+44 (0870) 0621894",
      "website": "https://www.mancity.com",
      "email": "mancity@mancity.com",
      "founded": 1880,
      "clubColors": "Sky Blue / White",
      "venue": "Etihad Stadium",
      "lastUpdated": "2020-11-19T02:08:13Z"
    },
    {
      "id": 503,
      "area": {
        "id": 2187,
        "name": "Portugal"
      },
      "name": "FC Porto",
      "shortName": "Porto",
      "tla": "FCP",
      "crestUrl": "https://crests.football-data.org/503.svg",
      "address": "Estádio do Dragão, Entrada Poente - Piso 3 Porto 4350-451",
      "phone": "+351 (022) 5070500",
      "website": "http://www.fcporto.pt",
      "email": "geral@portosad.pt",
      "founded": 1893,
      "clubColors": "Blue / White",
      "venue": "Estádio Do Dragão",
      "lastUpdated": "2020-11-19T02:15:37Z"
    },
    {
      "id": 654,
      "area": {
        "id": 2093,
        "name": "Greece"
      },
      "name": "PAE Olympiakos SFP",
      "shortName": "Olympiakos",
      "tla": "OLY",
      "crestUrl": "https://crests.football-data.org/654.svg",
      "address": "Plateia Alexandras Piraeus 18534",
      "phone": "+30 (210) 4143000",
      "website": "http://www.olympiacos.org",
      "email": "info@olympiacos.org",
      "founded": 1925,
      "clubColors": "Red / White",
      "venue": "Stadio Georgios Karaiskáki",
      "lastUpdated": "2020-11-19T02:17:04Z"
    },
    {
      "id": 516,
      "area": {
        "id": 2081,
        "name": "France"
      },
      "name": "Olympique de Marseille",
      "shortName": "Marseille",
      "tla": "OM",
      "crestUrl": "https://crests.football-data.org/516.svg",
      "address": "La Commanderie, 33, traverse de La Martine Marseille 13012",
      "phone": "+33 (049) 1765609",
      "website": "http://www.om.net",
      "email": "om@olympiquedemarseille.com",
      "founded": 1898,
      "clubColors": "White / Blue",
      "venue": "Orange Vélodrome",
      "lastUpdated": "2020-11-19T02:17:59Z"
    },
    {
      "id": 678,
      "area": {
        "id": 2163,
        "name": "Netherlands"
      },
      "name": "AFC Ajax",
      "shortName": "Ajax",
      "tla": "AJA",
      "crestUrl": "https://crests.football-data.org/678.svg",
      "address": "ArenA Boulevard 29 Amsterdam 1101 AX",
      "phone": "+31 (020) 3111444",
      "website": "http://www.ajax.nl",
      "email": "info@ajax.nl",
      "founded": 1900,
      "clubColors": "Red / White",
      "venue": "Johan Cruyff ArenA",
      "lastUpdated": "2020-11-19T02:07:20Z"
    },
    {
      "id": 64,
      "area": {
        "id": 2072,
        "name": "England"
      },
      "name": "Liverpool FC",
      "shortName": "Liverpool",
      "tla": "LIV",
      "crestUrl": "https://crests.football-data.org/64.svg",
      "address": "Anfield Road Liverpool L4 0TH",
      "phone": "+44 (0844) 4993000",
      "website": "http://www.liverpoolfc.tv",
      "email": "customercontact@liverpoolfc.tv",
      "founded": 1892,
      "clubColors": "Red / White",
      "venue": "Anfield",
      "lastUpdated": "2020-11-20T09:46:14Z"
    },
    {
      "id": 4485,
      "area": {
        "id": 2065,
        "name": "Denmark"
      },
      "name": "FC Midtjylland",
      "shortName": "Midtjylland",
      "tla": "MID",
      "crestUrl": "https://crests.football-data.org/4485.svg",
      "address": "Kaj Zartows Vej 5, Postboks 287 Herning 7400",
      "phone": "+45 (96) 271040",
      "website": "http://www.fcm.dk",
      "email": "fcmidtjylland@fc-mj.dk",
      "founded": 1999,
      "clubColors": "Black / Red",
      "venue": "MCH Arena",
      "lastUpdated": "2020-11-19T02:09:23Z"
    },
    {
      "id": 102,
      "area": {
        "id": 2114,
        "name": "Italy"
      },
      "name": "Atalanta BC",
      "shortName": "Atalanta",
      "tla": "ATA",
      "crestUrl": "https://crests.football-data.org/102.svg",
      "address": "Corso Europa, 46, Zingonia Ciserano 24040",
      "phone": "+39 (035) 4186211",
      "website": "http://www.atalanta.it",
      "email": "info@atalanta.it",
      "founded": 1904,
      "clubColors": "Black / Blue",
      "venue": "Stadio Atleti Azzurri d'Italia",
      "lastUpdated": "2020-11-19T02:14:47Z"
    }
  ]
};
