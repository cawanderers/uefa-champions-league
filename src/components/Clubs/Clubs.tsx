import React, {useEffect, useState} from 'react';
import ClubCard from '../ClubCard/ClubCard';
import footballData from "../../api/footballData";
import {UefaCompetitionId} from "../Matches/Matches";

const Clubs = () => {
  const [clubs, setClubs] = useState([]);

  useEffect(() => {
    const getClubs = async () => {
      const {data} = await footballData.get(`/competitions/${UefaCompetitionId}/teams?stage=GROUP_STAGE`);
      setClubs(data.teams);
    };
    getClubs();

  }, []);

  const clubCards = clubs.map(({id, shortName}: { id: number, shortName: string }) => (
    <ClubCard key={id} name={shortName} logo={`${process.env.PUBLIC_URL}/assets/images/ClubsPng/${id}.png`} />));

  return !clubs?.length ? <div>Loading Clubs...</div> : (
    <div className="container">
      <div className="row row-cols-6 text-center">
        {clubCards}
      </div>
    </div>
  );
};

export default Clubs;
