import React from 'react';
import {format, parseISO} from "date-fns";
import {Match, Score} from "./Matches/Matches";

type Props = Omit<Match, 'id'|'group'>

const FixtureResult = (match: Props) => {
  const {
    homeTeam: {id: homeTeamId, name: homeTeamName},
    awayTeam: {id: awayTeamId, name: awayTeamName},
    utcDate,
    status,
    score
  } = match;
  const getStatus = (status: string, score: Score) => (status === 'FINISHED' ? `${score.fullTime.homeTeam} : ${score.fullTime.awayTeam}` : '- : -');
  const formattedDate = format(parseISO(utcDate), 'yyyy-MM-dd');
  const homeTeamLogo = `${process.env.PUBLIC_URL}/assets/images/ClubsPng/${homeTeamId}.png`;
  const awayTeamLogo = `${process.env.PUBLIC_URL}/assets/images/ClubsPng/${awayTeamId}.png`;

  return (
    <>
      <span className="col mb-1">{homeTeamName}</span>
      <picture className="col mb-1"><img src={homeTeamLogo} alt={homeTeamName} /></picture>
      <span className="col mb-1">{getStatus(status, score)}</span>
      <picture className="col mb-1"><img src={awayTeamLogo} alt={awayTeamName} /></picture>
      <span className="col mb-1"> {awayTeamName}</span>
      <span className="col-2 mb-1">{formattedDate}</span>
    </>
  );
};

export default FixtureResult;
