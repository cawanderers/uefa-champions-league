import React from 'react';
import { Story } from '@storybook/react/types-6-0';

import GoalRanking from './GoalRanking';

export default {
  title: 'GoalRanking',
  component: GoalRanking,
};

const Template: Story<any> = (args) => (
  <GoalRanking {...args} />
);

export const Default = Template.bind({});
Default.args = {
  "scorers":[
    {
      "player":{
        "id":7819,
        "name":"Álvaro Morata",
        "firstName":"Álvaro Borja",
        "lastName":null,
        "dateOfBirth":"1992-10-23",
        "countryOfBirth":"Spain",
        "nationality":"Spain",
        "position":"Attacker",
        "shirtNumber":null,
        "lastUpdated":"2020-11-26T02:19:40Z"
      },
      "team":{
        "id":109,
        "name":"Juventus FC"
      },
      "numberOfGoals":6
    },
    {
      "player":{
        "id":38101,
        "name":"Erling Haaland",
        "firstName":"Erling Braut",
        "lastName":null,
        "dateOfBirth":"2000-07-21",
        "countryOfBirth":"England",
        "nationality":"Norway",
        "position":"Attacker",
        "shirtNumber":null,
        "lastUpdated":"2020-09-07T18:47:42Z"
      },
      "team":{
        "id":4,
        "name":"Borussia Dortmund"
      },
      "numberOfGoals":6
    },
    {
      "player":{
        "id":3331,
        "name":"Marcus Rashford",
        "firstName":"Marcus",
        "lastName":null,
        "dateOfBirth":"1997-10-31",
        "countryOfBirth":"England",
        "nationality":"England",
        "position":"Attacker",
        "shirtNumber":null,
        "lastUpdated":"2020-11-26T02:14:23Z"
      },
      "team":{
        "id":66,
        "name":"Manchester United FC"
      },
      "numberOfGoals":6
    },
    {
      "player":{
        "id":8491,
        "name":"Neymar Jr.",
        "firstName":"Neymar",
        "lastName":null,
        "dateOfBirth":"1992-02-05",
        "countryOfBirth":"Brazil",
        "nationality":"Brazil",
        "position":"Attacker",
        "shirtNumber":null,
        "lastUpdated":"2020-09-07T21:05:24Z"
      },
      "team":{
        "id":524,
        "name":"Paris Saint-Germain FC"
      },
      "numberOfGoals":6
    },
    {
      "player":{
        "id":2076,
        "name":"Ciro Immobile",
        "firstName":"Ciro",
        "lastName":null,
        "dateOfBirth":"1990-02-20",
        "countryOfBirth":"Italy",
        "nationality":"Italy",
        "position":"Attacker",
        "shirtNumber":null,
        "lastUpdated":"2020-11-26T02:19:41Z"
      },
      "team":{
        "id":110,
        "name":"SS Lazio"
      },
      "numberOfGoals":5
    },
    {
      "player":{
        "id":8443,
        "name":"Alassane Pléa",
        "firstName":"Alassane",
        "lastName":null,
        "dateOfBirth":"1993-03-10",
        "countryOfBirth":"France",
        "nationality":"France",
        "position":"Attacker",
        "shirtNumber":null,
        "lastUpdated":"2020-09-07T18:48:02Z"
      },
      "team":{
        "id":18,
        "name":"Borussia Mönchengladbach"
      },
      "numberOfGoals":5
    },
    {
      "player":{
        "id":3371,
        "name":"Olivier Giroud",
        "firstName":"Olivier",
        "lastName":null,
        "dateOfBirth":"1986-09-30",
        "countryOfBirth":"France",
        "nationality":"France",
        "position":"Attacker",
        "shirtNumber":null,
        "lastUpdated":"2020-11-26T02:14:18Z"
      },
      "team":{
        "id":61,
        "name":"Chelsea FC"
      },
      "numberOfGoals":5
    },
    {
      "player":{
        "id":16347,
        "name":"Dominik Szoboszlai",
        "firstName":"Dominik",
        "lastName":null,
        "dateOfBirth":"2000-10-25",
        "countryOfBirth":"Hungary",
        "nationality":"Hungary",
        "position":"Midfielder",
        "shirtNumber":null,
        "lastUpdated":"2020-08-20T03:19:06Z"
      },
      "team":{
        "id":1877,
        "name":"FC Red Bull Salzburg"
      },
      "numberOfGoals":4
    },
    {
      "player":{
        "id":3257,
        "name":"Bruno Fernandes",
        "firstName":"Bruno Miguel",
        "lastName":null,
        "dateOfBirth":"1994-09-08",
        "countryOfBirth":"Portugal",
        "nationality":"Portugal",
        "position":"Midfielder",
        "shirtNumber":null,
        "lastUpdated":"2020-09-07T21:02:46Z"
      },
      "team":{
        "id":66,
        "name":"Manchester United FC"
      },
      "numberOfGoals":4
    },
    {
      "player":{
        "id":3662,
        "name":"Romelu Lukaku",
        "firstName":"Romelu",
        "lastName":null,
        "dateOfBirth":"1993-05-13",
        "countryOfBirth":"Belgium",
        "nationality":"Belgium",
        "position":"Attacker",
        "shirtNumber":null,
        "lastUpdated":"2020-11-26T02:19:38Z"
      },
      "team":{
        "id":108,
        "name":"FC Internazionale Milano"
      },
      "numberOfGoals":4
    },
    {
      "player":{
        "id":33154,
        "name":"Ferrán",
        "firstName":"Ferrán",
        "lastName":null,
        "dateOfBirth":"2000-02-29",
        "countryOfBirth":"Spain",
        "nationality":"Spain",
        "position":"Attacker",
        "shirtNumber":null,
        "lastUpdated":"2020-09-07T21:02:45Z"
      },
      "team":{
        "id":65,
        "name":"Manchester City FC"
      },
      "numberOfGoals":4
    },
    {
      "player":{
        "id":49,
        "name":"Karim Benzema",
        "firstName":"Karim",
        "lastName":null,
        "dateOfBirth":"1987-12-19",
        "countryOfBirth":"France",
        "nationality":"France",
        "position":"Attacker",
        "shirtNumber":null,
        "lastUpdated":"2020-11-26T02:18:30Z"
      },
      "team":{
        "id":86,
        "name":"Real Madrid CF"
      },
      "numberOfGoals":4
    },
    {
      "player":{
        "id":4092,
        "name":"Diogo Jota",
        "firstName":"Diogo José",
        "lastName":null,
        "dateOfBirth":"1996-12-04",
        "countryOfBirth":"Portugal",
        "nationality":"Portugal",
        "position":"Attacker",
        "shirtNumber":null,
        "lastUpdated":"2020-09-07T21:02:52Z"
      },
      "team":{
        "id":64,
        "name":"Liverpool FC"
      },
      "numberOfGoals":4
    },
    {
      "player":{
        "id":10724,
        "name":"Mërgim Berisha",
        "firstName":"Mërgim",
        "lastName":null,
        "dateOfBirth":"1998-05-11",
        "countryOfBirth":"Albania",
        "nationality":"Germany",
        "position":"Attacker",
        "shirtNumber":null,
        "lastUpdated":"2020-08-20T03:19:06Z"
      },
      "team":{
        "id":1877,
        "name":"FC Red Bull Salzburg"
      },
      "numberOfGoals":4
    },
    {
      "player":{
        "id":32836,
        "name":"Youssef En-Nesyri",
        "firstName":"Youssef",
        "lastName":null,
        "dateOfBirth":"1997-06-01",
        "countryOfBirth":"Morocco",
        "nationality":"Morocco",
        "position":"Attacker",
        "shirtNumber":null,
        "lastUpdated":"2020-09-07T21:26:21Z"
      },
      "team":{
        "id":559,
        "name":"Sevilla FC"
      },
      "numberOfGoals":4
    },
    {
      "player":{
        "id":44,
        "name":"Cristiano Ronaldo",
        "firstName":"Cristiano Ronaldo",
        "lastName":null,
        "dateOfBirth":"1985-02-05",
        "countryOfBirth":"Portugal",
        "nationality":"Portugal",
        "position":"Attacker",
        "shirtNumber":null,
        "lastUpdated":"2020-11-26T02:19:40Z"
      },
      "team":{
        "id":109,
        "name":"Juventus FC"
      },
      "numberOfGoals":4
    },
    {
      "player":{
        "id":83795,
        "name":"Myrto Uzuni",
        "firstName":"Myrto",
        "lastName":null,
        "dateOfBirth":"1995-05-31",
        "countryOfBirth":"Albania",
        "nationality":"Albania",
        "position":"Attacker",
        "shirtNumber":77,
        "lastUpdated":"2020-09-03T04:19:44Z"
      },
      "team":{
        "id":5954,
        "name":"Ferencvárosi TC"
      },
      "numberOfGoals":3
    },
    {
      "player":{
        "id":24503,
        "name":"Anders Dreyer",
        "firstName":"Anders",
        "lastName":null,
        "dateOfBirth":"1998-05-02",
        "countryOfBirth":"Denmark",
        "nationality":"Denmark",
        "position":"Attacker",
        "shirtNumber":null,
        "lastUpdated":"2020-08-25T05:35:26Z"
      },
      "team":{
        "id":4485,
        "name":"FC Midtjylland"
      },
      "numberOfGoals":3
    },
    {
      "player":{
        "id":8541,
        "name":"Rémy Cabella",
        "firstName":"Rémy",
        "lastName":null,
        "dateOfBirth":"1990-03-08",
        "countryOfBirth":"France",
        "nationality":"France",
        "position":"Midfielder",
        "shirtNumber":7,
        "lastUpdated":"2020-09-03T04:07:31Z"
      },
      "team":{
        "id":5452,
        "name":"FK Krasnodar"
      },
      "numberOfGoals":3
    },
    {
      "player":{
        "id":38432,
        "name":"Franck Boli",
        "firstName":"Bi Sylvestre Franck",
        "lastName":null,
        "dateOfBirth":"1993-12-07",
        "countryOfBirth":"Côte d’Ivoire",
        "nationality":"Côte d’Ivoire",
        "position":"Attacker",
        "shirtNumber":70,
        "lastUpdated":"2020-09-03T04:19:44Z"
      },
      "team":{
        "id":5954,
        "name":"Ferencvárosi TC"
      },
      "numberOfGoals":3
    }
  ]
};
