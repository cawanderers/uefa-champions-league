import React, {useState, useEffect} from 'react';
import footballData from "../../api/footballData";
import {UefaCompetitionId} from "../Matches/Matches";
import {formatName, formatNationality, getPlayerImg} from "../../utils";
import PlayerAvatar from "../PlayerAvatar";

interface Team {
  id: number;
  name: string
}

interface PLayer {
  name: string,
  nationality: string
}

interface Scorer {
  numberOfGoals: number,
  player: PLayer,
  team: Team
}

const GoalRanking = () => {
  const [goalRanking, setGoalRanking] = useState([]);

  useEffect(() => {
    const getGoalRanking = async () => {
      const {data: {scorers: result}} = await footballData.get(`/competitions/${UefaCompetitionId}/scorers?limit=20`);
      setGoalRanking(result);
    };
    getGoalRanking();
  }, []);

  const playerGoalRanking = goalRanking.map((scorer: Scorer) => {
    const {
      numberOfGoals,
      player: {name: playerName, nationality: nation},
      team: {name: teamName}
    } = scorer;
    const formatPlayerName = formatName(playerName);
    const formatNation = formatNationality(nation);
    const playerImg = getPlayerImg(formatPlayerName, formatNation);

    return (
      <li key={playerName} className="row row-cols align-items-center">
        <span className="col-2 mb-1 ml-5">
          <PlayerAvatar img={playerImg} name={playerName} />
        </span>
        <span className="col mb-1">{playerName}</span>
        <span className="col mb-1"> {numberOfGoals}</span>
        <span className="col-2 mb-1">{teamName}</span>
      </li>
    );
  });
  return !goalRanking?.length ? <div>Loading Goal Ranking...</div> : (
    <ul className="container text-white text-center">
      {playerGoalRanking}
    </ul>
  );
};

export default GoalRanking;
