import React from 'react';
import { Story } from '@storybook/react/types-6-0';

import GroupDetailsMatches from "./GroupDetailsMatches";

export default  {
  title: 'GroupDetailsMatches',
  component: GroupDetailsMatches,
};

const Template: Story<any> = (args) => (
  <GroupDetailsMatches {...args} />
);

export const Default = Template.bind({});
Default.args = {
  "matches":[
    {
      "id":313161,
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6
      },
      "utcDate":"2020-10-21T16:55:00Z",
      "status":"FINISHED",
      "matchday":1,
      "stage":"GROUP_STAGE",
      "group":"Group A",
      "lastUpdated":"2020-10-22T01:35:02Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"DRAW",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":2,
          "awayTeam":2
        },
        "halfTime":{
          "homeTeam":1,
          "awayTeam":1
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":1877,
        "name":"FC Red Bull Salzburg"
      },
      "awayTeam":{
        "id":5455,
        "name":"FK Lokomotiv Moskva"
      },
      "referees":[
        {
          "id":43858,
          "name":"Serdar Gözübüyük",
          "role":"REF",
          "nationality":"Netherlands"
        },
        {
          "id":43860,
          "name":"Joost van Zuilen",
          "role":"REF",
          "nationality":"Netherlands"
        },
        {
          "id":14764,
          "name":"Johan Balder",
          "role":"REF",
          "nationality":"Netherlands"
        },
        {
          "id":56905,
          "name":"Bas Nijhuis",
          "role":"REF",
          "nationality":"Netherlands"
        },
        {
          "id":15152,
          "name":"Pol van Boekel",
          "role":"REF",
          "nationality":"Netherlands"
        },
        {
          "id":43861,
          "name":"Dennis Higler",
          "role":"REF",
          "nationality":"Netherlands"
        }
      ]
    },
    {
      "id":313162,
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6
      },
      "utcDate":"2020-10-21T19:00:00Z",
      "status":"FINISHED",
      "matchday":1,
      "stage":"GROUP_STAGE",
      "group":"Group A",
      "lastUpdated":"2021-01-06T14:30:10Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"HOME_TEAM",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":4,
          "awayTeam":0
        },
        "halfTime":{
          "homeTeam":2,
          "awayTeam":0
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":5,
        "name":"FC Bayern München"
      },
      "awayTeam":{
        "id":78,
        "name":"Club Atlético de Madrid"
      },
      "referees":[
        {
          "id":11605,
          "name":"Michael Oliver",
          "role":"REF",
          "nationality":"England"
        },
        {
          "id":11564,
          "name":"Stuart Burt",
          "role":"REF",
          "nationality":"England"
        },
        {
          "id":11488,
          "name":"Simon Bennett",
          "role":"REF",
          "nationality":"England"
        },
        {
          "id":11423,
          "name":"Andy Madley",
          "role":"REF",
          "nationality":"England"
        },
        {
          "id":11494,
          "name":"Stuart Attwell",
          "role":"REF",
          "nationality":"England"
        },
        {
          "id":11585,
          "name":"Craig Pawson",
          "role":"REF",
          "nationality":"England"
        }
      ]
    },
    {
      "id":313163,
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6
      },
      "utcDate":"2020-10-27T17:55:00Z",
      "status":"FINISHED",
      "matchday":2,
      "stage":"GROUP_STAGE",
      "group":"Group A",
      "lastUpdated":"2020-10-28T11:05:04Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"AWAY_TEAM",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":1,
          "awayTeam":2
        },
        "halfTime":{
          "homeTeam":0,
          "awayTeam":1
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":5455,
        "name":"FK Lokomotiv Moskva"
      },
      "awayTeam":{
        "id":5,
        "name":"FC Bayern München"
      },
      "referees":[
        {
          "id":37278,
          "name":"István Kovács",
          "role":"REF",
          "nationality":"Romania"
        },
        {
          "id":36700,
          "name":"Vasile Marinescu",
          "role":"REF",
          "nationality":"Romania"
        },
        {
          "id":36546,
          "name":"Mihai Artene",
          "role":"REF",
          "nationality":"Romania"
        },
        {
          "id":37162,
          "name":"Marius Avram",
          "role":"REF",
          "nationality":"Romania"
        },
        {
          "id":15625,
          "name":"Alejandro Hernández",
          "role":"REF",
          "nationality":"Spain"
        },
        {
          "id":55934,
          "name":"Guillermo Cuadra",
          "role":"REF",
          "nationality":"Spain"
        }
      ]
    },
    {
      "id":313164,
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6
      },
      "utcDate":"2020-10-27T20:00:00Z",
      "status":"FINISHED",
      "matchday":2,
      "stage":"GROUP_STAGE",
      "group":"Group A",
      "lastUpdated":"2020-10-28T11:30:05Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"HOME_TEAM",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":3,
          "awayTeam":2
        },
        "halfTime":{
          "homeTeam":1,
          "awayTeam":1
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":78,
        "name":"Club Atlético de Madrid"
      },
      "awayTeam":{
        "id":1877,
        "name":"FC Red Bull Salzburg"
      },
      "referees":[
        {
          "id":36314,
          "name":"Ovidiu Hațegan",
          "role":"REF",
          "nationality":"Romania"
        },
        {
          "id":36316,
          "name":"Octavian Șovre",
          "role":"REF",
          "nationality":"Romania"
        },
        {
          "id":36837,
          "name":"Sebastian Gheorghe",
          "role":"REF",
          "nationality":"Romania"
        },
        {
          "id":10977,
          "name":"Fabio Maresca",
          "role":"REF",
          "nationality":"Italy"
        },
        {
          "id":11060,
          "name":"Marco Di Bello",
          "role":"REF",
          "nationality":"Italy"
        },
        {
          "id":11075,
          "name":"Paolo Valeri",
          "role":"REF",
          "nationality":"Italy"
        }
      ]
    },
    {
      "id":313165,
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6
      },
      "utcDate":"2020-11-03T17:55:00Z",
      "status":"FINISHED",
      "matchday":3,
      "stage":"GROUP_STAGE",
      "group":"Group A",
      "lastUpdated":"2020-11-07T05:30:09Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"DRAW",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":1,
          "awayTeam":1
        },
        "halfTime":{
          "homeTeam":1,
          "awayTeam":1
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":5455,
        "name":"FK Lokomotiv Moskva"
      },
      "awayTeam":{
        "id":78,
        "name":"Club Atlético de Madrid"
      },
      "referees":[
        {
          "id":43926,
          "name":"Benoît Bastien",
          "role":"REF",
          "nationality":"France"
        },
        {
          "id":43885,
          "name":"Hicham Zakrani",
          "role":"REF",
          "nationality":"France"
        },
        {
          "id":43927,
          "name":"Frédéric Haquette",
          "role":"REF",
          "nationality":"France"
        },
        {
          "id":57051,
          "name":"Karim Abed",
          "role":"REF",
          "nationality":"France"
        },
        {
          "id":57042,
          "name":"Willy Delajod",
          "role":"REF",
          "nationality":"France"
        },
        {
          "id":30236,
          "name":"Abdulkadir Bitigen",
          "role":"REF",
          "nationality":"Turkey"
        }
      ]
    },
    {
      "id":313166,
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6
      },
      "utcDate":"2020-11-03T20:00:00Z",
      "status":"FINISHED",
      "matchday":3,
      "stage":"GROUP_STAGE",
      "group":"Group A",
      "lastUpdated":"2020-11-04T04:20:03Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"AWAY_TEAM",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":2,
          "awayTeam":6
        },
        "halfTime":{
          "homeTeam":1,
          "awayTeam":2
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":1877,
        "name":"FC Red Bull Salzburg"
      },
      "awayTeam":{
        "id":5,
        "name":"FC Bayern München"
      },
      "referees":[
        {
          "id":43899,
          "name":"Danny Makkelie",
          "role":"REF",
          "nationality":"Netherlands"
        },
        {
          "id":43874,
          "name":"Mario Diks",
          "role":"REF",
          "nationality":"Netherlands"
        },
        {
          "id":43900,
          "name":"Hessel Steegstra",
          "role":"REF",
          "nationality":"Netherlands"
        },
        {
          "id":11065,
          "name":"Daniele Doveri",
          "role":"REF",
          "nationality":"Italy"
        },
        {
          "id":11054,
          "name":"Massimiliano Irrati",
          "role":"REF",
          "nationality":"Italy"
        },
        {
          "id":57636,
          "name":"Vitaly Meshkov",
          "role":"REF",
          "nationality":"Russia"
        }
      ]
    },
    {
      "id":313167,
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6
      },
      "utcDate":"2020-11-25T20:00:00Z",
      "status":"FINISHED",
      "matchday":4,
      "stage":"GROUP_STAGE",
      "group":"Group A",
      "lastUpdated":"2021-01-06T14:30:11Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"HOME_TEAM",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":3,
          "awayTeam":1
        },
        "halfTime":{
          "homeTeam":1,
          "awayTeam":0
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":5,
        "name":"FC Bayern München"
      },
      "awayTeam":{
        "id":1877,
        "name":"FC Red Bull Salzburg"
      },
      "referees":[
        {
          "id":40663,
          "name":"Orel Grinfeeld",
          "role":"MAIN_REFEREE",
          "nationality":"Israel"
        },
        {
          "id":64594,
          "name":"Roy Hassan",
          "role":"ASSISTANT_N1",
          "nationality":"Israel"
        },
        {
          "id":41353,
          "name":"Idan Yarkoni",
          "role":"ASSISTANT_N2",
          "nationality":"Israel"
        },
        {
          "id":42060,
          "name":"Eitan Shmuelevich",
          "role":"FOURTH_OFFICIAL",
          "nationality":"Israel"
        }
      ]
    },
    {
      "id":313168,
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6
      },
      "utcDate":"2020-11-25T20:00:00Z",
      "status":"FINISHED",
      "matchday":4,
      "stage":"GROUP_STAGE",
      "group":"Group A",
      "lastUpdated":"2021-01-06T14:30:11Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"DRAW",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":0,
          "awayTeam":0
        },
        "halfTime":{
          "homeTeam":0,
          "awayTeam":0
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":78,
        "name":"Club Atlético de Madrid"
      },
      "awayTeam":{
        "id":5455,
        "name":"FK Lokomotiv Moskva"
      },
      "referees":[
        {
          "id":9351,
          "name":"Slavko Vinčič",
          "role":"MAIN_REFEREE",
          "nationality":"Slovenia"
        },
        {
          "id":9349,
          "name":"Tomaž Klančnik",
          "role":"ASSISTANT_N1",
          "nationality":"Slovenia"
        },
        {
          "id":43934,
          "name":"Andraž Kovačič",
          "role":"ASSISTANT_N2",
          "nationality":"Slovenia"
        },
        {
          "id":43936,
          "name":"Rade Obrenović",
          "role":"FOURTH_OFFICIAL",
          "nationality":"Slovenia"
        }
      ]
    },
    {
      "id":313169,
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6
      },
      "utcDate":"2020-12-01T17:55:00Z",
      "status":"FINISHED",
      "matchday":5,
      "stage":"GROUP_STAGE",
      "group":"Group A",
      "lastUpdated":"2020-12-02T02:15:08Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"AWAY_TEAM",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":1,
          "awayTeam":3
        },
        "halfTime":{
          "homeTeam":0,
          "awayTeam":2
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":5455,
        "name":"FK Lokomotiv Moskva"
      },
      "awayTeam":{
        "id":1877,
        "name":"FC Red Bull Salzburg"
      },
      "referees":[
        {
          "id":30784,
          "name":"Ali Palabıyık",
          "role":"MAIN_REFEREE",
          "nationality":"Turkey"
        },
        {
          "id":30680,
          "name":"Ceyhun Sesigüzel",
          "role":"ASSISTANT_N1",
          "nationality":"Turkey"
        },
        {
          "id":30786,
          "name":"Serkan Olguncan",
          "role":"ASSISTANT_N2",
          "nationality":"Turkey"
        },
        {
          "id":30698,
          "name":"Arda Kardeşler",
          "role":"FOURTH_OFFICIAL",
          "nationality":"Turkey"
        }
      ]
    },
    {
      "id":313170,
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6
      },
      "utcDate":"2020-12-01T20:00:00Z",
      "status":"FINISHED",
      "matchday":5,
      "stage":"GROUP_STAGE",
      "group":"Group A",
      "lastUpdated":"2021-01-06T14:30:11Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"DRAW",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":1,
          "awayTeam":1
        },
        "halfTime":{
          "homeTeam":1,
          "awayTeam":0
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":78,
        "name":"Club Atlético de Madrid"
      },
      "awayTeam":{
        "id":5,
        "name":"FC Bayern München"
      },
      "referees":[
        {
          "id":9374,
          "name":"Clément Turpin",
          "role":"MAIN_REFEREE",
          "nationality":"France"
        },
        {
          "id":43883,
          "name":"Nicolas Danos",
          "role":"ASSISTANT_N1",
          "nationality":"France"
        },
        {
          "id":43884,
          "name":"Cyril Gringore",
          "role":"ASSISTANT_N2",
          "nationality":"France"
        },
        {
          "id":57090,
          "name":"Franck Schneider",
          "role":"FOURTH_OFFICIAL",
          "nationality":"France"
        }
      ]
    },
    {
      "id":313171,
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6
      },
      "utcDate":"2020-12-09T20:00:00Z",
      "status":"FINISHED",
      "matchday":6,
      "stage":"GROUP_STAGE",
      "group":"Group A",
      "lastUpdated":"2021-01-06T14:30:11Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"HOME_TEAM",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":2,
          "awayTeam":0
        },
        "halfTime":{
          "homeTeam":0,
          "awayTeam":0
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":5,
        "name":"FC Bayern München"
      },
      "awayTeam":{
        "id":5455,
        "name":"FK Lokomotiv Moskva"
      },
      "referees":[
        {
          "id":31823,
          "name":"Sandro Schärer",
          "role":"MAIN_REFEREE",
          "nationality":"Switzerland"
        },
        {
          "id":14932,
          "name":"Stéphane De Almeida",
          "role":"ASSISTANT_N1",
          "nationality":"Switzerland"
        },
        {
          "id":30769,
          "name":"Bekim Zogaj",
          "role":"ASSISTANT_N2",
          "nationality":"Switzerland"
        },
        {
          "id":30810,
          "name":"Alain Bieri",
          "role":"FOURTH_OFFICIAL",
          "nationality":"Switzerland"
        },
        {
          "id":31257,
          "name":"Jonas Erni",
          "role":"ASSISTANT_N2",
          "nationality":"Switzerland"
        }
      ]
    },
    {
      "id":313172,
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6
      },
      "utcDate":"2020-12-09T20:00:00Z",
      "status":"FINISHED",
      "matchday":6,
      "stage":"GROUP_STAGE",
      "group":"Group A",
      "lastUpdated":"2021-01-06T14:30:11Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"AWAY_TEAM",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":0,
          "awayTeam":2
        },
        "halfTime":{
          "homeTeam":0,
          "awayTeam":1
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":1877,
        "name":"FC Red Bull Salzburg"
      },
      "awayTeam":{
        "id":78,
        "name":"Club Atlético de Madrid"
      },
      "referees":[
        {
          "id":11580,
          "name":"Anthony Taylor",
          "role":"MAIN_REFEREE",
          "nationality":"England"
        },
        {
          "id":11581,
          "name":"Gary Beswick",
          "role":"ASSISTANT_N1",
          "nationality":"England"
        },
        {
          "id":11615,
          "name":"Adam Nunn",
          "role":"ASSISTANT_N2",
          "nationality":"England"
        },
        {
          "id":11585,
          "name":"Craig Pawson",
          "role":"FOURTH_OFFICIAL",
          "nationality":"England"
        }
      ]
    }
  ]
};
