import React, {useEffect, useState} from 'react';
import footballData from "../../api/footballData";
import {Match, UefaCompetitionId} from "../Matches/Matches";
import FixtureResult from "../FixtureResult";

const GroupDetailsMatches = ({group}: { group: string }) => {
  const [groupMatches, setGroupMatches] = useState<Match[]>([]);

  useEffect(() => {
    const getGroupMatches = async () => {
      const {data: {matches: results}} = await footballData.get(`/competitions/${UefaCompetitionId}/matches?group=${group}`);
      setGroupMatches(results);
    };
    getGroupMatches();
  }, [group]);

  const groupMatchesList = groupMatches.map(({group, id, ...match}) => (
    <li key={id} className="row row-cols align-items-center mb-3">
      <FixtureResult {...match} />
    </li>
  ));

  return !groupMatches.length ? <div>Loading GROUP MATCHES...</div> : (
    <div>{groupMatchesList}</div>
  );
};

export default GroupDetailsMatches;
