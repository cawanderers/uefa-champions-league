import React from 'react';
import { Story } from '@storybook/react/types-6-0';

import GroupStanding from './GroupStanding';

export default {
  title: 'GroupStanding',
  component: GroupStanding,
}

const Template: Story<any> = (args: {groupId: string, table: any}) => (
  <GroupStanding {...args} />
);

export const Default = Template.bind({});
Default.args = {
  groupId: 'Group A',
  table: [
    {
      "position":1,
      "team":{
        "id":5,
        "name":"FC Bayern München",
        "crestUrl":"https://crests.football-data.org/5.svg"
      },
      "playedGames":6,
      "form":"W,D,W,W,W",
      "won":5,
      "draw":1,
      "lost":0,
      "points":16,
      "goalsFor":18,
      "goalsAgainst":5,
      "goalDifference":13
    },
    {
      "position":2,
      "team":{
        "id":78,
        "name":"Club Atlético de Madrid",
        "crestUrl":"https://crests.football-data.org/78.svg"
      },
      "playedGames":6,
      "form":"W,D,D,D,W",
      "won":2,
      "draw":3,
      "lost":1,
      "points":9,
      "goalsFor":7,
      "goalsAgainst":8,
      "goalDifference":-1
    },
    {
      "position":3,
      "team":{
        "id":1877,
        "name":"FC Red Bull Salzburg",
        "crestUrl":"https://crests.football-data.org/1877.svg"
      },
      "playedGames":6,
      "form":"L,W,L,L,L",
      "won":1,
      "draw":1,
      "lost":4,
      "points":4,
      "goalsFor":10,
      "goalsAgainst":17,
      "goalDifference":-7
    },
    {
      "position":4,
      "team":{
        "id":5455,
        "name":"FK Lokomotiv Moskva",
        "crestUrl":"https://crests.football-data.org/5455.svg"
      },
      "playedGames":6,
      "form":"L,L,D,D,L",
      "won":0,
      "draw":3,
      "lost":3,
      "points":3,
      "goalsFor":5,
      "goalsAgainst":10,
      "goalDifference":-5
    }
  ]
};
