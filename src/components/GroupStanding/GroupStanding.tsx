import React from 'react';
import StandingRow from "../StandingRow";
import {Accordion, Button, Card, Table} from "react-bootstrap";
import './GroupStanding.scss';
import GroupDetailsMatches from "../GroupDetailsMatches/GroupDetailsMatches";

const GroupStanding = ({groupId, table}: { groupId: string, table: any }) => {
  const formattedGroupId = groupId.replace(/_/g, ' ');
  const getGroupId = formattedGroupId.charAt(0) + formattedGroupId.slice(1,5).toLowerCase() + ' ' + formattedGroupId.charAt(6);
  interface teamInfo {
    id: number,
    name: string
  }

  interface TeamStanding {
    draw: number;
    goalDifference: number,
    goalsAgainst: number,
    goalsFor: number,
    lost: number,
    playedGames: number,
    points: number,
    position: number,
    won: number
    team: teamInfo
  }

  const getTable = table.length && table.map((teamStanding: TeamStanding) => {
    const {
      draw,
      goalDifference,
      goalsAgainst,
      goalsFor,
      lost,
      playedGames,
      points,
      position,
      won,
      team: {id: teamId, name: teamName}
    } = teamStanding;
    const teamLogo = `${process.env.PUBLIC_URL}/assets/images/ClubsPng/${teamId}.png`;

    return (
      <StandingRow key={position} teamName={teamName} teamLogo={teamLogo} games={playedGames} wins={won} draws={draw}
                   losses={lost} goal={goalsFor} against={goalsAgainst} difference={goalDifference} points={points} />
    );
  });

  return (
    <div className="text-white align-items-center">
      <span className="sticky-top group-id-header row">{getGroupId}</span>
      <Table responsive="sm" className="table table-dark text-center">
        <thead>
        <tr>
          <th scope="col">Team</th>
          <th scope="col">Games</th>
          <th scope="col">Wins</th>
          <th scope="col">Draws</th>
          <th scope="col">Losses</th>
          <th scope="col">For</th>
          <th scope="col">Against</th>
          <th scope="col">+/-</th>
          <th scope="col">Points</th>
        </tr>
        </thead>
        <tbody>
          {getTable}
        </tbody>
      </Table>
      <Accordion className="accordion-container">
        <Card className="bg-dark accordion-card">
          <Card.Header>
            <Accordion.Toggle className="text-white bg-dark accordion-button" as={Button} variant="link" eventKey="0">
              Group details
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="0">
            <Card.Body className="bg-dark text-center"><GroupDetailsMatches group={getGroupId} /></Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </div>
  );
};

export default GroupStanding;
