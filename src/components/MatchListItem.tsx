import React from 'react';
import {Match} from "./Matches/Matches";

const MatchListItem = ({match, homeLogo, awayLogo, date, status}: {match: Match, homeLogo: string, awayLogo: string, status: string, date: string}) => {
  const {group, homeTeam: {name: homeTeamName}, awayTeam: {name: awayTeamName}} = match;

  return (
    <li className="row row-cols align-items-center">
      <span className="col-2 mb-1 ml-5">{group}</span>
      <span className="col mb-1">{homeTeamName}</span>
      <span className="col mb-1"><img className="w-30" src={homeLogo} alt={homeTeamName} /></span>
      <span className="col mb-1">{status}</span>
      <span className="col mb-1"><img className="w-30" src={awayLogo} alt={awayTeamName} /></span>
      <span className="col mb-1"> {awayTeamName}</span>
      <span className="col-2 mb-1">{date}</span>
    </li>
  );
};

export default MatchListItem;
