import React from 'react';
import { Story } from '@storybook/react/types-6-0';

import Matches from "./Matches";

export default {
  title: 'Matches',
  component: Matches,
};

const Template: Story<any> = (args) => (
  <Matches {...args} />
);

export const Default = Template.bind({});
Default.args = {
  "matches":[
    {
      "id":313209,
      "competition":{
        "id":2001,
        "name":"UEFA Champions League",
        "area":{
          "name":"Europe",
          "code":"EUR",
          "ensignUrl":null
        }
      },
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6,
        "winner":null
      },
      "utcDate":"2020-10-20T19:00:00Z",
      "status":"FINISHED",
      "matchday":1,
      "stage":"GROUP_STAGE",
      "group":"Group E",
      "lastUpdated":"2020-11-07T05:30:12Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"DRAW",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":0,
          "awayTeam":0
        },
        "halfTime":{
          "homeTeam":0,
          "awayTeam":0
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":61,
        "name":"Chelsea FC"
      },
      "awayTeam":{
        "id":559,
        "name":"Sevilla FC"
      },
      "referees":[
        {
          "id":11029,
          "name":"Davide Massa",
          "role":"REF",
          "nationality":"Italy"
        },
        {
          "id":10992,
          "name":"Stefano Alassio",
          "role":"REF",
          "nationality":"Italy"
        },
        {
          "id":11048,
          "name":"Alberto Tegoni",
          "role":"REF",
          "nationality":"Italy"
        },
        {
          "id":11050,
          "name":"Giampaolo Calvarese",
          "role":"REF",
          "nationality":"Italy"
        },
        {
          "id":11060,
          "name":"Marco Di Bello",
          "role":"REF",
          "nationality":"Italy"
        },
        {
          "id":11043,
          "name":"Maurizio Mariani",
          "role":"REF",
          "nationality":"Italy"
        }
      ]
    },
    {
      "id":313211,
      "competition":{
        "id":2001,
        "name":"UEFA Champions League",
        "area":{
          "name":"Europe",
          "code":"EUR",
          "ensignUrl":null
        }
      },
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6,
        "winner":null
      },
      "utcDate":"2020-10-28T17:55:00Z",
      "status":"FINISHED",
      "matchday":2,
      "stage":"GROUP_STAGE",
      "group":"Group E",
      "lastUpdated":"2020-11-07T05:30:12Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"AWAY_TEAM",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":0,
          "awayTeam":4
        },
        "halfTime":{
          "homeTeam":0,
          "awayTeam":1
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":5452,
        "name":"FK Krasnodar"
      },
      "awayTeam":{
        "id":61,
        "name":"Chelsea FC"
      },
      "referees":[
        {
          "id":30784,
          "name":"Ali Palabıyık",
          "role":"REF",
          "nationality":"Turkey"
        },
        {
          "id":30680,
          "name":"Ceyhun Sesigüzel",
          "role":"REF",
          "nationality":"Turkey"
        },
        {
          "id":30786,
          "name":"Serkan Olguncan",
          "role":"REF",
          "nationality":"Turkey"
        },
        {
          "id":30698,
          "name":"Arda Kardeşler",
          "role":"REF",
          "nationality":"Turkey"
        },
        {
          "id":11054,
          "name":"Massimiliano Irrati",
          "role":"REF",
          "nationality":"Italy"
        },
        {
          "id":15093,
          "name":"Mete Kalkavan",
          "role":"REF",
          "nationality":"Turkey"
        }
      ]
    },
    {
      "id":313213,
      "competition":{
        "id":2001,
        "name":"UEFA Champions League",
        "area":{
          "name":"Europe",
          "code":"EUR",
          "ensignUrl":null
        }
      },
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6,
        "winner":null
      },
      "utcDate":"2020-11-04T20:00:00Z",
      "status":"FINISHED",
      "matchday":3,
      "stage":"GROUP_STAGE",
      "group":"Group E",
      "lastUpdated":"2020-11-07T05:30:12Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"HOME_TEAM",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":3,
          "awayTeam":0
        },
        "halfTime":{
          "homeTeam":2,
          "awayTeam":0
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":61,
        "name":"Chelsea FC"
      },
      "awayTeam":{
        "id":529,
        "name":"Stade Rennais FC 1901"
      },
      "referees":[
        {
          "id":43878,
          "name":"Felix Zwayer",
          "role":"REF",
          "nationality":"Germany"
        },
        {
          "id":43923,
          "name":"Thorsten Schiffner",
          "role":"REF",
          "nationality":"Germany"
        },
        {
          "id":43879,
          "name":"Marco Achmüller",
          "role":"REF",
          "nationality":"Germany"
        },
        {
          "id":43943,
          "name":"Tobias Stieler",
          "role":"REF",
          "nationality":"Germany"
        },
        {
          "id":9567,
          "name":"Sascha Stegemann",
          "role":"REF",
          "nationality":"Germany"
        },
        {
          "id":26174,
          "name":"Bibiana Steinhaus",
          "role":"REF",
          "nationality":"Germany"
        }
      ]
    },
    {
      "id":313215,
      "competition":{
        "id":2001,
        "name":"UEFA Champions League",
        "area":{
          "name":"Europe",
          "code":"EUR",
          "ensignUrl":null
        }
      },
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6,
        "winner":null
      },
      "utcDate":"2020-11-24T17:55:00Z",
      "status":"FINISHED",
      "matchday":4,
      "stage":"GROUP_STAGE",
      "group":"Group E",
      "lastUpdated":"2021-01-06T14:30:14Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"AWAY_TEAM",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":1,
          "awayTeam":2
        },
        "halfTime":{
          "homeTeam":0,
          "awayTeam":1
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":529,
        "name":"Stade Rennais FC 1901"
      },
      "awayTeam":{
        "id":61,
        "name":"Chelsea FC"
      },
      "referees":[
        {
          "id":9558,
          "name":"Björn Kuipers",
          "role":"MAIN_REFEREE",
          "nationality":"Netherlands"
        },
        {
          "id":9559,
          "name":"Sander van Roekel",
          "role":"ASSISTANT_N1",
          "nationality":"Netherlands"
        },
        {
          "id":9560,
          "name":"Erwin Zeinstra",
          "role":"ASSISTANT_N2",
          "nationality":"Netherlands"
        },
        {
          "id":43858,
          "name":"Serdar Gözübüyük",
          "role":"FOURTH_OFFICIAL",
          "nationality":"Netherlands"
        }
      ]
    },
    {
      "id":313218,
      "competition":{
        "id":2001,
        "name":"UEFA Champions League",
        "area":{
          "name":"Europe",
          "code":"EUR",
          "ensignUrl":null
        }
      },
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6,
        "winner":null
      },
      "utcDate":"2020-12-02T20:00:00Z",
      "status":"FINISHED",
      "matchday":5,
      "stage":"GROUP_STAGE",
      "group":"Group E",
      "lastUpdated":"2021-01-06T14:30:14Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"AWAY_TEAM",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":0,
          "awayTeam":4
        },
        "halfTime":{
          "homeTeam":0,
          "awayTeam":1
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":559,
        "name":"Sevilla FC"
      },
      "awayTeam":{
        "id":61,
        "name":"Chelsea FC"
      },
      "referees":[
        {
          "id":43540,
          "name":"Artur Soares Dias",
          "role":"MAIN_REFEREE",
          "nationality":"Portugal"
        },
        {
          "id":37626,
          "name":"Rui Tavares",
          "role":"ASSISTANT_N1",
          "nationality":"Portugal"
        },
        {
          "id":37714,
          "name":"Paulo Soares",
          "role":"ASSISTANT_N2",
          "nationality":"Portugal"
        },
        {
          "id":37862,
          "name":"Hugo Miguel",
          "role":"FOURTH_OFFICIAL",
          "nationality":"Portugal"
        }
      ]
    },
    {
      "id":313219,
      "competition":{
        "id":2001,
        "name":"UEFA Champions League",
        "area":{
          "name":"Europe",
          "code":"EUR",
          "ensignUrl":null
        }
      },
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6,
        "winner":null
      },
      "utcDate":"2020-12-08T20:00:00Z",
      "status":"FINISHED",
      "matchday":6,
      "stage":"GROUP_STAGE",
      "group":"Group E",
      "lastUpdated":"2020-12-09T03:10:05Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":"DRAW",
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":1,
          "awayTeam":1
        },
        "halfTime":{
          "homeTeam":1,
          "awayTeam":1
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":61,
        "name":"Chelsea FC"
      },
      "awayTeam":{
        "id":5452,
        "name":"FK Krasnodar"
      },
      "referees":[
        {
          "id":43894,
          "name":"Pavel Královec",
          "role":"MAIN_REFEREE",
          "nationality":"Czech Republic"
        },
        {
          "id":62627,
          "name":"Jan Paták",
          "role":"ASSISTANT_N1",
          "nationality":"Czech Republic"
        },
        {
          "id":15605,
          "name":"Tomáš Mokrusch",
          "role":"ASSISTANT_N2",
          "nationality":"Czech Republic"
        },
        {
          "id":62699,
          "name":"Miroslav Zelinka",
          "role":"FOURTH_OFFICIAL",
          "nationality":"Czech Republic"
        }
      ]
    },
    {
      "id":315754,
      "competition":{
        "id":2001,
        "name":"UEFA Champions League",
        "area":{
          "name":"Europe",
          "code":"EUR",
          "ensignUrl":null
        }
      },
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6,
        "winner":null
      },
      "utcDate":"2021-02-23T20:00:00Z",
      "status":"SCHEDULED",
      "matchday":null,
      "stage":"ROUND_OF_16",
      "group":null,
      "lastUpdated":"2020-12-14T19:30:18Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":null,
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "halfTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":78,
        "name":"Club Atlético de Madrid"
      },
      "awayTeam":{
        "id":61,
        "name":"Chelsea FC"
      },
      "referees":[

      ]
    },
    {
      "id":315755,
      "competition":{
        "id":2001,
        "name":"UEFA Champions League",
        "area":{
          "name":"Europe",
          "code":"EUR",
          "ensignUrl":null
        }
      },
      "season":{
        "id":642,
        "startDate":"2020-08-08",
        "endDate":"2021-05-29",
        "currentMatchday":6,
        "winner":null
      },
      "utcDate":"2021-03-17T20:00:00Z",
      "status":"SCHEDULED",
      "matchday":null,
      "stage":"ROUND_OF_16",
      "group":null,
      "lastUpdated":"2020-12-14T19:30:18Z",
      "odds":{
        "msg":"Activate Odds-Package in User-Panel to retrieve odds."
      },
      "score":{
        "winner":null,
        "duration":"REGULAR",
        "fullTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "halfTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "extraTime":{
          "homeTeam":null,
          "awayTeam":null
        },
        "penalties":{
          "homeTeam":null,
          "awayTeam":null
        }
      },
      "homeTeam":{
        "id":61,
        "name":"Chelsea FC"
      },
      "awayTeam":{
        "id":78,
        "name":"Club Atlético de Madrid"
      },
      "referees":[

      ]
    }
  ]
};
