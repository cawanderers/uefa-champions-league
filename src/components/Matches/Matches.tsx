import React, {useState, useEffect} from 'react';
import footballData from "../../api/footballData";
// import MatchListItem from "./MatchListItem";
import {Carousel} from "react-bootstrap";
import './Matches.scss';
import FixtureResult from "../FixtureResult";

interface TimeStats {
  homeTeam: number,
  awayTeam: number
}

export interface Score {
  duration: string,
  extraTime: TimeStats,
  fullTime: TimeStats,
  halfTime: TimeStats,
  penalties: TimeStats,
  winner: string
}

interface TeamSummary {
  id: number,
  name: string
}

interface Competition {
  area: any,
  id: number,
  name: string
}

export interface Match {
  awayTeam: TeamSummary,
  competition: Competition,
  group: string,
  homeTeam: TeamSummary,
  id: number,
  matchday: number,
  referees: any,
  score: any,
  season: any,
  stage: string,
  status: 'FINISHED' | 'SCHEDULED',
  utcDate: string
}

export const UefaCompetitionId = 2001;

const Matches = () => {
  const [matches, setMatches] = useState([]);
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex: number) => {
    setIndex(selectedIndex);
  };

  useEffect(() => {
    const getMatches = async () => {
      const {data: {matches: results}} = await footballData.get('/teams/61/matches');
      setMatches(results);
    };
    getMatches();
  }, []);

  const champMatches: Match[] = matches.filter(({competition}: { competition: Competition }) => (competition.id === UefaCompetitionId));
  const matchesListView = champMatches.map(({group, id, ...match}) => (
    <Carousel.Item key={id}>
      {/*<MatchListItem key={id} match={match} homeLogo={homeTeamLogo} awayLogo={awayTeamLogo}*/}
      {/*               status={getStatus(status, score)} date={formattedDate} />*/}
      <div className="row row-cols align-items-center text-white w-100 h-75">
        <span className="col-2 mb-1 ml-5 group-id">{group}</span>
        <FixtureResult {...match} />
      </div>
    </Carousel.Item>
  ));

  return !matches?.length ? <div>Loading Matches...</div> : (
    <Carousel className="container" activeIndex={index} onSelect={handleSelect}>
      {matchesListView}
    </Carousel>
    // <ul className="container text-white text-center">
    //   {matchesListView}
    // </ul>
  );
};

export default Matches;
