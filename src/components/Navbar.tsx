import React from 'react';
import './Navbar.scss';

const Navbar = () => {
  const navItems = [
    {
      label: 'Latest',
      href: '#',
      value: 'latest'
    },
    {
      label: 'Fixtures & Results',
      href: '#',
      value: 'fixturesAndResults'
    },
    {
      label: 'Standings',
      href: '#',
      value: 'standings'
    },
    {
      label: 'Clubs',
      href: '#',
      value: 'clubs'
    },
    {
      label: 'Stats',
      href: '#',
      value: 'stats'
    },
    {
      label: 'History',
      href: '#',
      value: 'history'
    },
    {
      label: 'About',
      href: '#',
      value: 'about'
    }
  ];

  const navOptions = navItems.map(({label, href, value}: { label: string, href: string, value: string }) => (
    <li className="nav-item" key={value}><a href={href} className="nav-link">{label}</a></li>));
  return (
    <div className="container">
      <a href="/" className="logo">
        <img className="logoImg"
             src="https://img.uefa.com/imgml/uefacom/ucl/logo.svg"
             alt="UCL logo" />
      </a>
      <nav id="mainNavbar" className="navbar navbar-dark navbar-expand-lg">
        <button className="navbar-toggler" data-toggle="collapse" data-target="#navLinks"
                aria-label="Toggle Navigation">
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navLinks">
          <ul className="navbar-nav">
            {navOptions}
          </ul>
        </div>
      </nav>
      <h1 className="text-center">2019/20 UEFA GROUP STAGE TEAMS</h1>
    </div>
  );
};

export default Navbar;
