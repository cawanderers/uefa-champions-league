import React, {useState} from 'react';
import "./PlayerAvatar.scss";

type Props = {
  img: string,
  name: string
}

const defaultSrc = 'https://cdn.footystats.org/img/players/england-nathaniel-phillips.png';

const PlayerAvatar = ({img, name}: Props) => {
  const [hasError, setHasError] = useState(false);

  const onError = () => {
    setHasError(true);
  };

  return (
    <>
      {
        hasError ?
          <img src={defaultSrc} alt={name} /> :
          <img className="player-avatar-img" src={img} onError={onError} alt={name} />
      }
    </>
  );
};

export default PlayerAvatar;
