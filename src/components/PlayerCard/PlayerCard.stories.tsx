import React from 'react';
import { Story } from '@storybook/react/types-6-0';

import PlayerCard from './PlayerCard';

export default {
  title: 'PlayerCard',
  component: PlayerCard,
};

const Template: Story<any> = (args: {name: string, position: string, nationality: string, birthday: string}) => (
  <PlayerCard {...args} />
);

export const Default = Template.bind({});
Default.args = {
  name: 'Sadio Mané', position: 'Attacker', nationality: 'Senegal', birthday: '1992-04-09'
};
