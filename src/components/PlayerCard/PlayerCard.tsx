import React from 'react';
import PlayerAvatar from "../PlayerAvatar";

const PlayerCard = ({name, position, nationality, birthday}: { name: string, position: string, nationality: string, birthday: string }) => {
  const normalizedName = name.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
  const lowerCaseName = normalizedName.toLowerCase();
  const formattedName = lowerCaseName.replace(/ /g, '-');
  const country = nationality.toLowerCase();
  const logo = `https://cdn.footystats.org/img/players/${country}-${formattedName}.png`;

  return (
    <div className="col card mt-5 ml-4 mr-2" style={{width: "18rem"}}>
      <PlayerAvatar img={logo} name={name} />
      <div className="card-body text-center">
        {name}
      </div>
      <ul className="list-group list-group-flush">
        <li className="list-group-item text-center">{position}</li>
        <li className="list-group-item text-center">{nationality}</li>
        <li className="list-group-item text-center">{birthday}</li>
      </ul>
    </div>
  );
};

export default PlayerCard;
