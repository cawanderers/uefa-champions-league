import React from 'react';
import { Story } from '@storybook/react/types-6-0';

import Players from './Players';

export default {
  title: 'Players',
  component: Players,
};

const Template: Story<any> = (args) => (
  <Players {...args} />
);

export const Default = Template.bind({});
Default.args = {
  "squad":[
    {
      "id":356,
      "name":"Thiago Alcântara",
      "position":"Midfielder",
      "dateOfBirth":"1991-04-11T00:00:00Z",
      "countryOfBirth":"Italy",
      "nationality":"Spain",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":1795,
      "name":"Alisson",
      "position":"Goalkeeper",
      "dateOfBirth":"1992-10-02T00:00:00Z",
      "countryOfBirth":"Brazil",
      "nationality":"Brazil",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":3233,
      "name":"Roberto Firmino",
      "position":"Attacker",
      "dateOfBirth":"1991-10-02T00:00:00Z",
      "countryOfBirth":"Brazil",
      "nationality":"Brazil",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":3320,
      "name":"Jordan Henderson",
      "position":"Midfielder",
      "dateOfBirth":"1990-06-17T00:00:00Z",
      "countryOfBirth":"England",
      "nationality":"England",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":3323,
      "name":"Alex Oxlade-Chamberlain",
      "position":"Midfielder",
      "dateOfBirth":"1993-08-15T00:00:00Z",
      "countryOfBirth":"England",
      "nationality":"England",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":3626,
      "name":"Sadio Mané",
      "position":"Attacker",
      "dateOfBirth":"1992-04-10T00:00:00Z",
      "countryOfBirth":"Senegal",
      "nationality":"Senegal",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":3663,
      "name":"Divock Origi",
      "position":"Attacker",
      "dateOfBirth":"1995-04-18T00:00:00Z",
      "countryOfBirth":"Belgium",
      "nationality":"Belgium",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":3754,
      "name":"Mohamed Salah",
      "position":"Attacker",
      "dateOfBirth":"1992-06-15T00:00:00Z",
      "countryOfBirth":"Egypt",
      "nationality":"Egypt",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":4068,
      "name":"Ovie Ejaria",
      "position":"Midfielder",
      "dateOfBirth":"1997-11-18T00:00:00Z",
      "countryOfBirth":"England",
      "nationality":"England",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":4092,
      "name":"Diogo Jota",
      "position":"Attacker",
      "dateOfBirth":"1996-12-04T00:00:00Z",
      "countryOfBirth":"Portugal",
      "nationality":"Portugal",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":7383,
      "name":"Kostas Tsimikas",
      "position":"Defender",
      "dateOfBirth":"1996-05-12T00:00:00Z",
      "countryOfBirth":"Greece",
      "nationality":"Greece",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":7720,
      "name":"Sepp van den Berg",
      "position":"Defender",
      "dateOfBirth":"2001-12-20T00:00:00Z",
      "countryOfBirth":"Netherlands",
      "nationality":"Netherlands",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":7861,
      "name":"James Milner",
      "position":"Midfielder",
      "dateOfBirth":"1986-01-04T00:00:00Z",
      "countryOfBirth":"England",
      "nationality":"England",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":7862,
      "name":"Joe Gomez",
      "position":"Defender",
      "dateOfBirth":"1997-05-23T00:00:00Z",
      "countryOfBirth":"England",
      "nationality":"England",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":7865,
      "name":"Joël Matip",
      "position":"Defender",
      "dateOfBirth":"1991-08-08T00:00:00Z",
      "countryOfBirth":"Germany",
      "nationality":"Cameroon",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":7867,
      "name":"Trent Alexander-Arnold",
      "position":"Defender",
      "dateOfBirth":"1998-10-07T00:00:00Z",
      "countryOfBirth":"England",
      "nationality":"England",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":7868,
      "name":"Andrew Robertson",
      "position":"Defender",
      "dateOfBirth":"1994-03-11T00:00:00Z",
      "countryOfBirth":"Scotland",
      "nationality":"Scotland",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":7869,
      "name":"Virgil van Dijk",
      "position":"Defender",
      "dateOfBirth":"1991-07-08T00:00:00Z",
      "countryOfBirth":"Netherlands",
      "nationality":"Netherlands",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":7871,
      "name":"Georginio Wijnaldum",
      "position":"Midfielder",
      "dateOfBirth":"1990-11-11T00:00:00Z",
      "countryOfBirth":"Netherlands",
      "nationality":"Netherlands",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":7873,
      "name":"Curtis Jones",
      "position":"Midfielder",
      "dateOfBirth":"2001-01-30T00:00:00Z",
      "countryOfBirth":"England",
      "nationality":"England",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":7955,
      "name":"Xherdan Shaqiri",
      "position":"Midfielder",
      "dateOfBirth":"1991-10-10T00:00:00Z",
      "countryOfBirth":"Kosovo",
      "nationality":"Switzerland",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":8197,
      "name":"Adrián",
      "position":"Goalkeeper",
      "dateOfBirth":"1987-01-03T00:00:00Z",
      "countryOfBirth":"Spain",
      "nationality":"Spain",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":8749,
      "name":"Fabinho",
      "position":"Midfielder",
      "dateOfBirth":"1993-10-23T00:00:00Z",
      "countryOfBirth":"Brazil",
      "nationality":"Brazil",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":9344,
      "name":"Jürgen Klopp",
      "position":null,
      "dateOfBirth":"1967-06-16T00:00:00Z",
      "countryOfBirth":"Germany",
      "nationality":"Germany",
      "shirtNumber":null,
      "role":"COACH"
    },
    {
      "id":9547,
      "name":"Naby Keïta",
      "position":"Midfielder",
      "dateOfBirth":"1995-02-10T00:00:00Z",
      "countryOfBirth":"Guinea",
      "nationality":"Guinea",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":15100,
      "name":"Takumi Minamino",
      "position":"Attacker",
      "dateOfBirth":"1995-01-16T00:00:00Z",
      "countryOfBirth":"Japan",
      "nationality":"Japan",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":102046,
      "name":"Caoimhin Kelleher",
      "position":"Goalkeeper",
      "dateOfBirth":"1998-11-23T00:00:00Z",
      "countryOfBirth":"Republic of Ireland",
      "nationality":"Republic of Ireland",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":125215,
      "name":"Nathaniel Phillips",
      "position":"Defender",
      "dateOfBirth":"1997-03-21T00:00:00Z",
      "countryOfBirth":"England",
      "nationality":"England",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":131118,
      "name":"Leighton Clarkson",
      "position":"Midfielder",
      "dateOfBirth":"2001-10-19T00:00:00Z",
      "countryOfBirth":"England",
      "nationality":"England",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":133765,
      "name":"Neco Williams",
      "position":"Defender",
      "dateOfBirth":"2001-04-13T00:00:00Z",
      "countryOfBirth":"Wales",
      "nationality":"Wales",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":133767,
      "name":"Billy Koumetio",
      "position":"Defender",
      "dateOfBirth":"2002-11-14T00:00:00Z",
      "countryOfBirth":"France",
      "nationality":"France",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":135636,
      "name":"Rhys Williams",
      "position":"Defender",
      "dateOfBirth":"2001-02-03T00:00:00Z",
      "countryOfBirth":"England",
      "nationality":"England",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":141285,
      "name":"Jake Cain",
      "position":"Midfielder",
      "dateOfBirth":"2001-09-02T00:00:00Z",
      "countryOfBirth":"England",
      "nationality":"England",
      "shirtNumber":null,
      "role":"PLAYER"
    },
    {
      "id":152579,
      "name":"Vitezslav Jaros",
      "position":"Goalkeeper",
      "dateOfBirth":"2001-07-23T00:00:00Z",
      "countryOfBirth":"Czech Republic",
      "nationality":"Czech Republic",
      "shirtNumber":null,
      "role":"PLAYER"
    }
  ]
};
