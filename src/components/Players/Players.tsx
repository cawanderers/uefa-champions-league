import React, {useEffect, useState} from 'react';
import footballData from "../../api/footballData";
import {format, parseISO} from 'date-fns';
import PlayerCard from '../PlayerCard/PlayerCard';

interface Player {
  countryOfBirth: string,
  dateOfBirth: string,
  id: number,
  name: string,
  nationality: string,
  position: string,
  role: string
}

const Players = () => {
  const [squad, setSquad] = useState([]);

  useEffect(() => {
    const getSquad = async () => {
      const {data} = await footballData.get('/teams/64');
      setSquad(data.squad);
    };
    getSquad();

  }, []);

  const players: Player[] = squad.filter(({role}: { role: string }) => (role === 'PLAYER'));
  const playerCards = players.map(({id, name, position, nationality, dateOfBirth}) => {
    const formattedBirthday = format(parseISO(dateOfBirth), 'yyyy-MM-dd');

    return (
      <PlayerCard key={id} name={name} position={position} nationality={nationality} birthday={formattedBirthday} />
    );
  });

  return !squad?.length ? <div>Loading PLayers...</div> : (
    <div className="container">
      <div className="row row-cols-6">
        {playerCards}
      </div>
    </div>
  );
}
export default Players;
