import React from 'react';
import './StandingRow.scss'

const StandingRow = ({
                       teamName,
                       teamLogo,
                       games,
                       wins,
                       draws,
                       losses,
                       goal,
                       against,
                       difference,
                       points
                     }: { teamName: string, teamLogo: string, games: number, wins: number, draws: number, losses: number, goal: number, against: number, difference: number, points: number }) => {

  return (
    <tr className="standing-row">
      <td className="row">
        <span className="col-2"><img className="mw-100" src={teamLogo} alt={teamName} /></span>
        <span className="col-7 text-left align-self-center"><span>{teamName}</span></span>
      </td>
      <td className="align-middle">{games}</td>
      <td className="align-middle">{wins}</td>
      <td className="align-middle">{draws}</td>
      <td className="align-middle">{losses}</td>
      <td className="align-middle">{goal}</td>
      <td className="align-middle">{against}</td>
      <td className="align-middle">{difference}</td>
      <td className="align-middle">{points}</td>
    </tr>
  );
};

export default StandingRow;
