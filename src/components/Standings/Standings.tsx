import React, {useState, useEffect} from 'react';
import footballData from "../../api/footballData";
import GroupStanding from "../GroupStanding/GroupStanding";
import {UefaCompetitionId} from "../Matches/Matches";

interface Standing {
  stage: string,
  type: string,
  group: string,
  table: any
}

const Standings = () => {
  const [standings, setStandings] = useState([]);

  useEffect(() => {
    const getStandings = async () => {
      const {data} = await footballData.get(`/competitions/${UefaCompetitionId}/standings`);
      setStandings(data.standings);
    };
    getStandings();

  }, []);

  const getGroupStandings = () => {
    const totalStandings: Standing[] = standings.filter(({type}) => type === 'TOTAL');
    return totalStandings.map(({group, table}) => (
    <GroupStanding key={group} groupId={group} table={table} />
  ));}

  return !standings.length ? <div>Loading Standings...</div> : (
    <div className="container">
      {getGroupStandings()}
    </div>
  );
};

export default Standings;
