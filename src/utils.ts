import {format, parseISO} from "date-fns";

export function getTeamLogo(teamId: number) {
  return `${process.env.PUBLIC_URL}/assets/images/ClubsPng/${teamId}.png`;
}

export function convertUTCDate(utcDate: string) {
  return format(parseISO(utcDate), 'yyyy-MM-dd');
}

export function formatScores(homeTeamScore: number, awayTeamScore: number) {
  return `${homeTeamScore} : ${awayTeamScore}`;
}

export function formatName(name: string) {
  return name.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase().replace(/ /g, '-');
}

export function formatNationality(nationality: string) {
  return nationality.toLowerCase();
}

export function getPlayerImg(playerName: string, nationality: string) {
  return `https://cdn.footystats.org/img/players/${formatNationality(nationality)}-${formatName(playerName)}.png`;
}
